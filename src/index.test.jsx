import ReactDOM from 'react-dom';
jest.mock('react-dom', ()=> ({render: jest.fn()}))
import './index'

it('renders without crashing', () => {
  expect(ReactDOM.render).toHaveBeenCalled()
});
