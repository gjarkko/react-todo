#!/usr/bin/env node
// Add project node_modules/.bin to path in a unique and environment independent way
var path = require('path');
var delimiter = path.delimiter;
var path_parts = process.env.PATH.split(delimiter);
var project_path = path.join(process.cwd(), 'node_modules', '.bin');
if(!path_parts.includes(project_path)) {
   path_parts.unshift(project_path);
}
console.log(path_parts.filter((part, i, list) => list.indexOf(part) === i).join(delimiter));
